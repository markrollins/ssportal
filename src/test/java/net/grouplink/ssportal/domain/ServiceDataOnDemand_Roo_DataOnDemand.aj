// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.grouplink.ssportal.domain;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import net.grouplink.ssportal.domain.Service;
import net.grouplink.ssportal.domain.ServiceDataOnDemand;
import net.grouplink.ssportal.domain.ServiceType;
import net.grouplink.ssportal.domain.ServiceTypeDataOnDemand;
import net.grouplink.ssportal.domain.Tenant;
import net.grouplink.ssportal.domain.TenantDataOnDemand;
import net.grouplink.ssportal.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

privileged aspect ServiceDataOnDemand_Roo_DataOnDemand {
    
    declare @type: ServiceDataOnDemand: @Component;
    
    private Random ServiceDataOnDemand.rnd = new SecureRandom();
    
    private List<Service> ServiceDataOnDemand.data;
    
    @Autowired
    ServiceTypeDataOnDemand ServiceDataOnDemand.serviceTypeDataOnDemand;
    
    @Autowired
    TenantDataOnDemand ServiceDataOnDemand.tenantDataOnDemand;
    
    @Autowired
    ServiceRepository ServiceDataOnDemand.serviceRepository;
    
    public Service ServiceDataOnDemand.getNewTransientService(int index) {
        Service obj = new Service();
        setName(obj, index);
        setServiceType(obj, index);
        setTenant(obj, index);
        return obj;
    }
    
    public void ServiceDataOnDemand.setName(Service obj, int index) {
        String name = "name_" + index;
        obj.setName(name);
    }
    
    public void ServiceDataOnDemand.setServiceType(Service obj, int index) {
        ServiceType serviceType = serviceTypeDataOnDemand.getRandomServiceType();
        obj.setServiceType(serviceType);
    }
    
    public void ServiceDataOnDemand.setTenant(Service obj, int index) {
        Tenant tenant = tenantDataOnDemand.getRandomTenant();
        obj.setTenant(tenant);
    }
    
    public Service ServiceDataOnDemand.getSpecificService(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        Service obj = data.get(index);
        Long id = obj.getId();
        return serviceRepository.findOne(id);
    }
    
    public Service ServiceDataOnDemand.getRandomService() {
        init();
        Service obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return serviceRepository.findOne(id);
    }
    
    public boolean ServiceDataOnDemand.modifyService(Service obj) {
        return false;
    }
    
    public void ServiceDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = serviceRepository.findAll(new org.springframework.data.domain.PageRequest(from / to, to)).getContent();
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'Service' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<Service>();
        for (int i = 0; i < 10; i++) {
            Service obj = getNewTransientService(i);
            try {
                serviceRepository.save(obj);
            } catch (final ConstraintViolationException e) {
                final StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    final ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
                }
                throw new IllegalStateException(msg.toString(), e);
            }
            serviceRepository.flush();
            data.add(obj);
        }
    }
    
}

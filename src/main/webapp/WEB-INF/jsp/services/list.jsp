<%@include file="/WEB-INF/jsp/includes.jsp" %>


<head>
    <title>Services</title>
    <script type="text/javascript">
        $(function () {
            // Set the menu bar item for this page active
            $('.navbar-nav a:contains("Services")').parent().addClass('active');

            $(".show_btn").on("click", function () {
                var serviceId = $(this).data("serviceid");
                location.href = "<spring:url value="/services/"/>" + serviceId;
            });

            $(".edit_btn").on("click", function () {
                var serviceId = $(this).data("serviceid");
                location.href = "<spring:url value="/services/"/>" + serviceId + "/edit";
            });

            $(".delete_btn").on("click", function () {
                var serviceId = $(this).data("serviceid");
                if (confirm('Are you sure you want to delete this item?')) {
                    $.ajax({
                        url: '<spring:url value="/services/"/>' + serviceId,
                        contentType: 'text/html',
                        type: 'DELETE',
                        success: function () {
                            $(this).parents("tr").remove();
                        }
                    });
                    location.reload();
                }
            });

            $(".new_btn").on("click", function () {
                location.href = "<spring:url value="/services/new"/>";
            });

        });
    </script>
</head>
<body>
<h1>
    <span class="label label-default">Services</span>
</h1>
<table class="table table-striped table-hover">
    <thead>
    <th>#</th>
    <th>Name</th>
    <th>Service Type</th>
    <th>Tenant</th>
    </thead>
    <tbody>
    <c:forEach items="${services}" var="service" varStatus="count">
        <tr>
            <td>${count.index+1}</td>
            <td><c:out value="${service.name}"/></td>
            <td><c:out value="${service.serviceType.name}"/></td>
            <td><c:out value="${service.tenant.name}"/></td>
            <td>
                <div class="pull-right">
                    <button title="Show" class="btn btn-default show_btn" data-serviceid="${service.id}">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </button>

                    <button title="Edit" class="btn btn-default edit_btn" data-serviceid="${service.id}">
                        <span class="glyphicon glyphicon-edit"></span>
                    </button>
                    <button title="Delete" class="btn btn-default delete_btn" data-serviceid="${service.id}">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </div>
            </td>
        </tr>
    </c:forEach>
    <tr class="footer">
        <td colspan="5">
            <button title="Create new Service" class="btn btn-default new_btn">
                <span class="glyphicon glyphicon-plus"></span>
            </button>

            <span class="pull-right">
            <c:if test="${not empty maxPages}">
                <util:pagination maxPages="${maxPages}" page="${param.page}" size="${param.size}"/>
            </c:if>
            </span>
        </td>
    </tr>
    </tbody>

</table>
</body>

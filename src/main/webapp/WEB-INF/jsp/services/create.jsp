<%@include file="/WEB-INF/jsp/includes.jsp" %>

<head>
    <title>Create New Service</title>
</head>

<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Create new Service</h3>
    </div>
    <div class="panel-body">
        <spring:url value="/services" var="action"/>
        <form:form commandName="service" method="post" action="${action}" cssClass="form-horizontal">
            <div class="form-group">
                <form:label path="name" cssClass="col-sm-2 control-label">Name</form:label>
                <div class="col-sm-5">
                    <form:input cssClass="form-control" path="name"/>
                </div>
            </div>
            <div class="form-group">
                <form:label path="serviceType" cssClass="col-sm-2 control-label">Service Type</form:label>
                <div class="col-sm-5">
                    <form:select cssClass="form-control" path="serviceType">
                        <form:options items="${serviceTypes}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </div>
            </div>
            <div class="form-group">
                <form:label path="tenant" class="col-sm-2 control-label">Tenant</form:label>
                <div class="col-sm-5">
                    <form:select cssClass="form-control" path="tenant">
                        <form:options items="${tenants}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </div>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form:form>
    </div>
</div>
</body>
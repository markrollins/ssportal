<%@include file="/WEB-INF/jsp/includes.jsp" %>

<head>
    <title>Service</title>

    <script type="text/javascript">
        $(function () {
            $(".list_btn").on("click", function () {
                location.href = "<spring:url value="/services"/>";
            });


            $(".edit_btn").on("click", function () {
                var serviceId = $(this).data("serviceid");
                location.href = "<spring:url value="/services/"/>" + serviceId + "/edit";
            });

            $(".delete_btn").on("click", function () {
                var serviceId = $(this).data("serviceid");
                if (confirm('Are you sure you want to delete this item?')) {
                    $.ajax({
                        url: '<spring:url value="/services/"/>' + serviceId,
                        contentType: 'text/html',
                        type: 'DELETE'

                    });
                    location.href = "<spring:url value="/services"/>";
                }
            });

        });
    </script>
</head>

<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Service</h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>

                <div class="col-sm-5">
                    <p class="form-control-static"><c:out value="${service.name}"/></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Service Type</label>

                <div class="col-sm-5">
                    <p class="form-control-static"><c:out value="${service.serviceType.name}"/></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tenant</label>
                <div class="col-sm-5">
                    <p class="form-control-static"><c:out value="${service.tenant.name}"/></p>
                </div>
            </div>
        </form>

        <button title="Back to Services" class="btn btn-default list_btn">
            <span class="glyphicon glyphicon-list"></span>
        </button>
        <button title="Edit" class="btn btn-default edit_btn" data-serviceid="${service.id}">
            <span class="glyphicon glyphicon-edit"></span>
        </button>
        <button title="Delete" class="btn btn-default delete_btn" data-serviceid="${service.id}">
            <span class="glyphicon glyphicon-trash"></span>
        </button>

    </div>
</div>
</body>
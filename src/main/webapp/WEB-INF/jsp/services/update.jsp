<%@include file="/WEB-INF/jsp/includes.jsp" %>

<head>
    <title>Update Service</title>
</head>

<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Update Service</h3>
    </div>
    <div class="panel-body">
        <spring:url value="/services/${service.id}" var="action"/>
        <form:form commandName="service" method="put" action="${action}" cssClass="form-horizontal">
            <div class="form-group">
                <form:label path="name" cssClass="col-sm-2 control-label">Name</form:label>
                <div class="col-sm-5">
                    <form:input path="name" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <form:label path="serviceType" cssClass="col-sm-2 control-label">Service Type</form:label>
                <div class="col-sm-5">
                    <form:select cssClass="form-control" path="serviceType">
                        <form:options items="${serviceTypes}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </div>
            </div>
            <div class="form-group">
                <form:label path="tenant" cssClass="col-sm-2 control-label">Tenant</form:label>
                <div class="col-sm-5">
                    <form:select cssClass="form-control" path="tenant">
                        <form:options items="${tenants}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </div>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form:form>
    </div>
</div>
</body>



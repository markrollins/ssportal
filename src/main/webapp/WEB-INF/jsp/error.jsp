<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="http.status.${statusCode}.name" text="Error Occurred"/></title>
</head>
<body class="special-page code-page dark">
<!--[if lt IE 9]><div class="ie"><![endif]-->
<!--[if lt IE 8]><div class="ie7"><![endif]-->

<h1>${statusCode}</h1>
<p><spring:message code="http.status.${statusCode}.name" text="Error Occurred"/></p>
<section>
    <ul class="action-tabs">
        <li><a href="javascript:history.back()" title="<spring:message code="global.goBack" text="Go back"/>"><img src="<c:url value="/images/theme/icons/fugue/navigation-180.png"/>" width="16" height="16"></a></li>
    </ul>

    <ul class="action-tabs right">
        <li><a href="<c:url value="/"/>" title="<spring:message code="global.goHome" text="Go home"/>"><img src="<c:url value="/images/theme/icons/fugue/home.png"/>" width="16" height="16"></a></li>
    </ul>

    <div class="block-content no-title dark-bg">

        <spring:message code="http.status.${statusCode}.description" text="An error occurred while processing your request"/>
    </div>
</section>
<!--[if lt IE 8]></div><![endif]-->
<!--[if lt IE 9]></div><![endif]-->
</body>
</html>



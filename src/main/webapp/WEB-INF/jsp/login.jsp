<%@ include file="/WEB-INF/jsp/includes.jsp" %>


<spring:url value="/resources/j_spring_security_check" var="form_url"/>
<form name="f" class="form-signin" action="${fn:escapeXml(form_url)}" method="POST">

    <h1>
        <img src="<c:out value="${tenant.logo}"/>"/>
        <span class="label label-default"><c:out value="${tenant.name}"/></span>

        <c:out value="${tenant.name}"/>
    </h1>

    <h2 class="form-signin-heading">Please sign in</h2>
    <input id="j_username" type='text' name='j_username' class="form-control" placeholder="Email address" required
           autofocus>
    <input id="j_password" type='password' name='j_password' class="form-control" placeholder="Password" required>
    <label class="checkbox">
        <input type="checkbox" value="remember-me"> Remember me
    </label>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <br/>

    <c:if test="${not empty param.login_error}">
        <div class="alert alert-danger">
            <spring:message code="security_login_unsuccessful"/>
            <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.
        </div>
    </c:if>
</form>

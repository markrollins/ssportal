<%@include file="/WEB-INF/jsp/includes.jsp" %>

<head>
    <title>Create New Service Request</title>

</head>

<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Create new Service Request</h3>
    </div>
    <div class="panel-body">
        <spring:url value="/servicerequests" var="action"/>
        <form:form commandName="serviceRequest" method="post" action="${action}" cssClass="form-horizontal">
            <div class="form-group">
                <form:label path="service" cssClass="col-sm-2 control-label">Service</form:label>
                <div class="col-sm-5">
                    <form:select cssClass="form-control" path="service">
                        <form:options items="${services}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </div>
            </div>
            <div class="form-group">
                <form:label path="user" cssClass="col-sm-2 control-label">User</form:label>
                <div class="col-sm-5">
                    <form:select cssClass="form-control" path="user">
                        <form:options items="${users}" itemLabel="displayName" itemValue="id"/>
                    </form:select>
                </div>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form:form>
    </div>
</div>
</body>
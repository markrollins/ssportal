<%@include file="/WEB-INF/jsp/includes.jsp" %>


<head>
    <title>Service Requests</title>
    <script type="text/javascript">
        $(function () {
            $(".show_btn").on("click", function () {
                var srid = $(this).data("srid");
                location.href = "<spring:url value="/servicerequests/"/>" + srid;
            });

            $(".edit_btn").on("click", function () {
                var srid = $(this).data("srid");
                location.href = "<spring:url value="/servicerequests/"/>" + srid + "/edit";
            });

            $(".delete_btn").on("click", function () {
                var srid = $(this).data("srid");
                if (confirm('Are you sure you want to delete this item?')) {
                    $.ajax({
                        url: '<spring:url value="/servicerequests/"/>' + srid,
                        contentType: 'text/html',
                        type: 'DELETE',
                        success: function(){
                            $(this).parents("tr").remove();
                        }
                    });
                    location.reload();
                }
            });

            $(".new_btn").on("click", function(){
                location.href = "<spring:url value="/servicerequests/new"/>";
            });
        });
    </script>
</head>
<body>

<h1>
    <span class="label label-default">Service</span>
    Requests
</h1>
<table class="table table-striped table-hover">
    <thead>
    <th>#</th>
    <th>Service</th>
    <th>User</th>
    </thead>
    <tbody>
    <c:forEach items="${servicerequests}" var="servicerequest" varStatus="count">
        <tr>
            <td>${count.index + 1}</td>
            <td><c:out value="${servicerequest.service.name}"/></td>
            <td>
                <c:out value="${servicerequest.user.lastName}"/>,
                <c:out value="${servicerequest.user.firstName}"/>
            </td>
            <td>
                <div class="pull-right">
                    <button title="Show" class="btn btn-default show_btn" data-srid="${servicerequest.id}">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </button>

                    <button title="Edit" class="btn btn-default edit_btn" data-srid="${servicerequest.id}">
                        <span class="glyphicon glyphicon-edit"></span>
                    </button>
                    <button title="Delete" class="btn btn-default delete_btn" data-srid="${servicerequest.id}">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </div>
            </td>
        </tr>
    </c:forEach>
    <tr class="footer">
        <td colspan="4">
            <button title="Create new Service Request" class="btn btn-default new_btn">
                <span class="glyphicon glyphicon-plus"></span>
            </button>

            <span class="pull-right">
            <c:if test="${not empty maxPages}">
                <util:pagination maxPages="${maxPages}" page="${param.page}" size="${param.size}" />
            </c:if>
            </span>
        </td>
    </tr>
    </tbody>
</table>
</body>

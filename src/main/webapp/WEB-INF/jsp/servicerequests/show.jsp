<%@include file="/WEB-INF/jsp/includes.jsp" %>

<head>
    <title>Service Request</title>

    <script type="text/javascript">
        $(function () {
            $(".list_btn").on("click", function () {
                location.href = "<spring:url value="/servicerequests"/>";
            });
            $(".edit_btn").on("click", function () {
                var srid = $(this).data("srid");
                location.href = "<spring:url value="/servicerequests/"/>" + srid + "/edit";
            });
            $(".delete_btn").on("click", function () {
                var srid = $(this).data("srid");
                if (confirm('Are you sure you want to delete this item?')) {
                    $.ajax({
                        url: '<spring:url value="/servicerequests/"/>' + srid,
                        contentType: 'text/html',
                        type: 'DELETE'
                    });
                    location.href = "<spring:url value="/servicerequests"/>";
                }
            });
        });
    </script>
</head>

<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Service Request</h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Service</label>
                <div class="col-sm-5">
                    <p class="form-control-static"><c:out value="${serviceRequest.service.name}"/></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">User</label>
                <div class="col-sm-5">
                    <p class="form-control-static"><c:out value="${serviceRequest.user.displayName}"/></p>
                </div>
            </div>
        </form>
        <button title="Back to Service Requests" class="btn btn-default list_btn">
            <span class="glyphicon glyphicon-list"></span>
        </button>
        <button title="Edit this Service Request" class="btn btn-default edit_btn" data-srid="${serviceRequest.id}">
            <span class="glyphicon glyphicon-edit"></span>
        </button>
        <button title="Delete this Service Request" class="btn btn-default delete_btn" data-srid="${serviceRequest.id}">
            <span class="glyphicon glyphicon-trash"></span>
        </button>
    </div>
</div>
</body>
<%@include file="/WEB-INF/jsp/includes.jsp" %>

<head>
    <title>Update Service Request</title>

</head>

<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Update Service Request</h3>
    </div>
    <div class="panel-body">
        <spring:url value="/servicerequests/${serviceRequest.id}" var="action"/>
        <form:form commandName="serviceRequest" method="put" action="${action}" cssClass="form-horizontal">
            <div class="form-group">
                <form:label path="service" cssClass="col-sm-2 control-label">Service</form:label>
                <div class="col-sm-5">
                    <form:select path="service" cssClass="form-control">
                        <form:options items="${services}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </div>
            </div>
            <div class="form-group">
                <form:label path="user" cssClass="col-sm-2 control-label">User</form:label>
                <div class="col-sm-5">
                    <form:select path="user" cssClass="form-control">
                        <form:options items="${users}" itemValue="id" itemLabel="displayName"/>
                    </form:select>
                </div>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form:form>
    </div>
</div>
</body>



<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@include file="/WEB-INF/jsp/includes.jsp"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><decorator:title/></title>

    <%--boostrap--%>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css">

    <!-- Custom styles for this template -->
    <link href="<c:url value="/resources/styles/sticky-footer-navbar.css"/>" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <decorator:head/>
</head>

<body>

<!-- Wrap all page content here -->
<div id="wrap">

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value="/"/>">SS Portal</a>
            </div>

            <c:set var="page" value="1"/>
            <c:set var="size" value="${empty param.size ? 10 :param.size}"/>

            <c:url value="/servicerequests" var="serviceRequestsListUrl">
                <c:param name="page" value="${page}"/>
                <c:param name="size" value="${size}"/>
            </c:url>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value="/"/>">Home</a></li>
                    <li><a href="<c:url value="/services"/>">Services</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header">Service Request</li>
                            <li><a href="<c:url value="/servicerequests/new"/>">Create new Service Request</a></li>
                            <li><a href="<c:out value="${serviceRequestsListUrl}"/>">List all Service Requests</a></li>
                            <li class="divider"></li>
                            <li class="dropdown-header">Service Type</li>
                            <li><a href="#">Create new Service Type</a></li>
                            <li><a href="#">List all Service Types</a></li>
                            <li class="divider"></li>
                            <li class="dropdown-header">Tenant</li>
                            <li><a href="#">Create new Tenant</a></li>
                            <li><a href="#">List all Tenants</a></li>
                            <li class="divider"></li>
                            <li class="dropdown-header">User</li>
                            <li><a href="#">Create new User</a></li>
                            <li><a href="#">List all Users</a></li>
                            <li><a href="#">List all Users</a></li>
                        </ul>
                    </li>
                    <li>
                        <spring:url value="/resources/j_spring_security_logout" var="logout"/>
                        <a href="${logout}">
                            <spring:message code="security_logout"/>
                        </a>

                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Begin page content -->
    <div class="container">
        <%--<div class="page-header">--%>
        <%--<h1>Sticky footer with fixed navbar</h1>--%>
        <%--</div>--%>
        <%--<p class="lead">Pin a fixed-height footer to the bottom of the viewport in desktop browsers with this custom HTML and CSS. A fixed navbar has been added within <code>#wrap</code> with <code>padding-top: 60px;</code> on the <code>.container</code>.</p>--%>
        <%--<p>Back to <a href="../sticky-footer">the default sticky footer</a> minus the navbar.</p>--%>

        <!-- decorator body -->
        <decorator:body/>
        <!-- end decorator body -->
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="text-muted credit">GroupLink Self Service Portal v 0.0.1</p>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

</body>
</html>

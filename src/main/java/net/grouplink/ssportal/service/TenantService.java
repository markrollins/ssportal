package net.grouplink.ssportal.service;
import net.grouplink.ssportal.domain.Tenant;
import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { net.grouplink.ssportal.domain.Tenant.class })
public interface TenantService {
    Tenant findBySubdomainIgnoreCase(String subdomain);
}

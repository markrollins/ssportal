package net.grouplink.ssportal.service;

import net.grouplink.ssportal.domain.Tenant;
import net.grouplink.ssportal.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class TenantServiceImpl implements TenantService {
    @Autowired
    private TenantRepository tenantRepository;

    public Tenant findBySubdomainIgnoreCase(String subdomain) {
        return tenantRepository.findBySubdomainIgnoreCase(subdomain);
    }
}

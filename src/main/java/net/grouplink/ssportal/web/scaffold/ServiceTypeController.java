package net.grouplink.ssportal.web.scaffold;
import net.grouplink.ssportal.domain.ServiceType;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/scaffold/servicetypes")
@Controller
@RooWebScaffold(path = "scaffold/servicetypes", formBackingObject = ServiceType.class)
public class ServiceTypeController {
}

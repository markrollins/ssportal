package net.grouplink.ssportal.web.scaffold;
import net.grouplink.ssportal.domain.ServiceRequest;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/scaffold/servicerequests")
@Controller("scaffoldServiceRequestController")
@RooWebScaffold(path = "scaffold/servicerequests", formBackingObject = ServiceRequest.class)
public class ServiceRequestController {
}

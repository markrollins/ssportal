package net.grouplink.ssportal.web.scaffold;
import net.grouplink.ssportal.domain.Service;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/scaffold/services")
@Controller("scaffoldServiceController")
@RooWebScaffold(path = "scaffold/services", formBackingObject = Service.class)
public class ServiceController {
}

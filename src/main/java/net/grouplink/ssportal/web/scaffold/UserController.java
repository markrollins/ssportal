package net.grouplink.ssportal.web.scaffold;
import net.grouplink.ssportal.domain.User;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/scaffold/users")
@Controller
@RooWebScaffold(path = "scaffold/users", formBackingObject = User.class)
public class UserController {
}

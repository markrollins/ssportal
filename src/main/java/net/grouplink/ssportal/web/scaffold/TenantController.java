package net.grouplink.ssportal.web.scaffold;
import net.grouplink.ssportal.domain.Tenant;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/scaffold/tenants")
@Controller
@RooWebScaffold(path = "scaffold/tenants", formBackingObject = Tenant.class)
public class TenantController {
}

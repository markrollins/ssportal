package net.grouplink.ssportal.web.interceptor;

import net.grouplink.ssportal.domain.Tenant;
import net.grouplink.ssportal.repository.TenantRepository;
import net.grouplink.ssportal.service.TenantService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * User: jhafen
 * Date: 11/22/13
 * Time: 2:56 PM
 */
public class TenantInterceptor extends HandlerInterceptorAdapter {

    final Log log = LogFactory.getLog(getClass());

    @PersistenceContext
    transient EntityManager entityManager;

    @Autowired
    private TenantService tenantService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("request.getRequestURI() = " + request.getRequestURI());
        log.debug("preHandle");

        String subdomain = request.getServerName().split("\\.")[0];

        Tenant tenant = tenantService.findBySubdomainIgnoreCase(subdomain);
        request.getSession().setAttribute("TENANT", tenant);
        if (tenant == null) {
            log.debug("no tenant found for subdomain " + subdomain);
        } else {
            try {
                log.debug("attempting to apply filters");
                log.debug("tenant.getName() = " + tenant.getName());

                HibernateEntityManager hibernateEntityManager = entityManager.unwrap(HibernateEntityManager.class);
                Session session = hibernateEntityManager.getSession();

                session.enableFilter("filterByTenant").setParameter("tenantId", tenant.getId());
            } catch (Exception e) {
                log.error("error applying filter", e);
                e.printStackTrace();
            }
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        log.debug("postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        log.debug("afterCompletion");
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        log.debug("afterConcurrentHandlingStarted");
    }
}

package net.grouplink.ssportal.web.controllers;

import net.grouplink.ssportal.domain.Service;
import net.grouplink.ssportal.domain.ServiceRequest;
import net.grouplink.ssportal.repository.ServiceRepository;
import net.grouplink.ssportal.repository.ServiceTypeRepository;
import net.grouplink.ssportal.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jhafen
 * Date: 11/29/13
 * Time: 10:13 AM
 */
@Controller
@RequestMapping("/services")
public class ServiceController {

    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    ServiceTypeRepository serviceTypeRepository;
    @Autowired
    TenantService tenantService;

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            model.addAttribute("services", serviceRepository.findAll(new org.springframework.data.domain.PageRequest(firstResult / sizeNo, sizeNo)).getContent());
            float nrOfPages = (float) serviceRepository.count() / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            model.addAttribute("services", serviceRepository.findAll());
        }
        return "services/list";
    }

    @RequestMapping(value = "/new", produces = "text/html")
    public String createForm(Model model){
        populateEditForm(model, new Service());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (serviceTypeRepository.count() == 0) {
            dependencies.add(new String[] { "servicetype", "servicetiypes" });
        }
        if (tenantService.countAllTenants() == 0) {
            dependencies.add(new String[] { "tenant", "scaffold/tenants" });
        }
        model.addAttribute("dependencies", dependencies);
        return "services/create";
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Service service, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            populateEditForm(model, service);
            return "services/create";
        }
        model.asMap().clear();
        serviceRepository.save(service);
        return "redirect:/services/" + service.getId().toString();
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model model) {
        model.addAttribute("service", serviceRepository.findOne(id));
        model.addAttribute("itemId", id);
        return "services/show";
    }

    @RequestMapping(value="/{id}", method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid @ModelAttribute("id") Service service, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            populateEditForm(model, service);
            return "services/update";
        }
        model.asMap().clear();
        serviceRepository.save(service);
        return "redirect:/services/" + service.getId().toString();
    }

    @RequestMapping(value = "/{id}/edit", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model model) {
        populateEditForm(model, serviceRepository.findOne(id));
        return "services/update";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public void delete(@PathVariable("id") Long id) {
        Service service = serviceRepository.findOne(id);
        serviceRepository.delete(service);
    }

    void populateEditForm(Model model, Service service) {
        model.addAttribute("service", service);
        model.addAttribute("serviceTypes", serviceTypeRepository.findAll());
        model.addAttribute("tenants", tenantService.findAllTenants());
    }

}

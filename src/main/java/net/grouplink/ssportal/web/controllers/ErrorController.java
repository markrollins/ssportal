package net.grouplink.ssportal.web.controllers;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/errors")
public class ErrorController {
    private Log log = LogFactory.getLog(getClass());

//    @Autowired
//    private UserService userService;

    @RequestMapping("/throwable")
    public String errorThrowable(HttpServletRequest request, Model model) {
        String errorRequestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
        String forwardRequestUri = (String) request.getAttribute("javax.servlet.forward.request_uri");
        String errorMessage = (String) request.getAttribute("javax.servlet.error.message");
        Integer errorStatusCode =  (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (errorStatusCode == null) {
            errorStatusCode = 500;
        }
        Class exceptionType = (Class) request.getAttribute("javax.servlet.error.exception_type");
        Throwable exception = (Throwable) request.getAttribute("javax.servlet.error.exception");

//        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
//        logError(errorRequestUri, errorStatusCode, errorMessage, exception, user);
        logError(errorRequestUri, errorStatusCode, errorMessage, exception);
        model.addAttribute("statusCode", errorStatusCode);
        return "error";
    }

    @RequestMapping(value = "/{statusCode}")
    public String errorStatus(@PathVariable Integer statusCode, HttpServletRequest request, Model model) {
        String errorRequestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
        String forwardRequestUri = (String) request.getAttribute("javax.servlet.forward.request_uri");
        String errorMessage = (String) request.getAttribute("javax.servlet.error.message");
        Integer errorStatusCode =  (Integer) request.getAttribute("javax.servlet.error.status_code");
        Class exceptionType = (Class) request.getAttribute("javax.servlet.error.exception_type");
        Throwable exception = (Throwable) request.getAttribute("javax.servlet.error.exception");

        if (errorStatusCode != null && !errorStatusCode.equals(statusCode)) {
            model.addAttribute("statusCode", errorStatusCode);
        } else {
            model.addAttribute("statusCode", statusCode);
        }

//        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
//        logError(errorRequestUri, statusCode, errorMessage, exception, user);
        logError(errorRequestUri, statusCode, errorMessage, exception);
        return "error";
    }

//    private void logError(String errorRequestUri, Integer errorStatusCode, String errorMessage, Throwable exception, User user) {
    private void logError(String errorRequestUri, Integer errorStatusCode, String errorMessage, Throwable exception) {
        StringBuilder sb = new StringBuilder().append("Error occurred for request uri [")
                .append(errorRequestUri).append("] - http status [").append(errorStatusCode).append("] - user [");

//        if (user != null) {
//            sb.append(user.getLoginId());
//        } else {
            sb.append("null");
//        }

        sb.append("]");
        if (StringUtils.isNotBlank(errorMessage)) {
            sb.append(" - error message [").append(errorMessage).append("]");
        }

        if (exception != null) {
            log.error(sb.toString(), exception);
        } else {
            log.error(sb.toString());
        }
    }
}

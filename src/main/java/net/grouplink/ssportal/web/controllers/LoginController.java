package net.grouplink.ssportal.web.controllers;

import net.grouplink.ssportal.domain.Tenant;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: mrollins
 * Date: 12/4/13
 * Time: 12:59 PM
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController {

    @RequestMapping(method = RequestMethod.GET)
    public String showLoginPage(HttpServletRequest request, Model model){
        Tenant tenant = (Tenant) request.getSession().getAttribute("TENANT");
        model.addAttribute("tenant", tenant);

        return "login";
    }

}

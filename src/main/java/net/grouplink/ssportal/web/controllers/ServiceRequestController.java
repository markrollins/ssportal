package net.grouplink.ssportal.web.controllers;

import net.grouplink.ssportal.domain.ServiceRequest;
import net.grouplink.ssportal.repository.ServiceRepository;
import net.grouplink.ssportal.repository.ServiceRequestRepository;
import net.grouplink.ssportal.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mrollins
 * Date: 12/9/13
 * Time: 1:31 PM
 */

@Controller
@RequestMapping("/servicerequests")
public class ServiceRequestController {

    @Autowired
    ServiceRequestRepository serviceRequestRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size;
            final int firstResult = page == null ? 0 : (page - 1) * sizeNo;
            model.addAttribute("servicerequests", serviceRequestRepository.findAll(new org.springframework.data.domain.PageRequest(firstResult / sizeNo, sizeNo)).getContent());
            float nrOfPages = (float) serviceRequestRepository.count() / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            model.addAttribute("servicerequests", serviceRequestRepository.findAll());
        }
        return "servicerequests/list";
    }

    @RequestMapping(value = "/new", produces = "text/html")
    public String createForm(Model model){
        populateEditForm(model, new ServiceRequest());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (serviceRepository.count() == 0) {
            dependencies.add(new String[] { "service", "services" });
        }
        model.addAttribute("dependencies", dependencies);
        return "servicerequests/create";
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid ServiceRequest serviceRequest, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            populateEditForm(model, serviceRequest);
            return "servicerequests/create";
        }
        model.asMap().clear();
        serviceRequestRepository.save(serviceRequest);
        return "redirect:/servicerequests/" + serviceRequest.getId().toString();
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model model) {
        model.addAttribute("serviceRequest", serviceRequestRepository.findOne(id));
        model.addAttribute("itemId", id);
        return "servicerequests/show";
    }


    @RequestMapping(value="/{id}", method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid @ModelAttribute("id") ServiceRequest serviceRequest, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            populateEditForm(model, serviceRequest);
            return "servicerequests/update";
        }
        model.asMap().clear();
        serviceRequestRepository.save(serviceRequest);
        return "redirect:/servicerequests/" + serviceRequest.getId().toString();
    }

    @RequestMapping(value = "/{id}/edit", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model model) {
        populateEditForm(model, serviceRequestRepository.findOne(id));
        return "servicerequests/update";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public void delete(@PathVariable("id") Long id) {
        ServiceRequest serviceRequest = serviceRequestRepository.findOne(id);
        serviceRequestRepository.delete(serviceRequest);
    }

    void populateEditForm(Model model, ServiceRequest serviceRequest) {
        model.addAttribute("serviceRequest", serviceRequest);
        model.addAttribute("services", serviceRepository.findAll());
        model.addAttribute("users", userRepository.findAll());
    }
}

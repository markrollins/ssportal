package net.grouplink.ssportal.repository;
import net.grouplink.ssportal.domain.ServiceRequest;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = ServiceRequest.class)
public interface ServiceRequestRepository {
}

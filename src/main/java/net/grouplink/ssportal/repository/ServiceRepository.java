package net.grouplink.ssportal.repository;
import net.grouplink.ssportal.domain.Service;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = Service.class)
public interface ServiceRepository {
}

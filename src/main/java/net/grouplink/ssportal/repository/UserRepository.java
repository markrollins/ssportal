package net.grouplink.ssportal.repository;
import net.grouplink.ssportal.domain.User;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = User.class)
public interface UserRepository {
}

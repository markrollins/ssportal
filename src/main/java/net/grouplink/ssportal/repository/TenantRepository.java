package net.grouplink.ssportal.repository;
import net.grouplink.ssportal.domain.Tenant;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = Tenant.class)
public interface TenantRepository {
    Tenant findBySubdomainIgnoreCase(String subdomain);
}

// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.grouplink.ssportal.repository;

import net.grouplink.ssportal.domain.ServiceRequest;
import net.grouplink.ssportal.repository.ServiceRequestRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

privileged aspect ServiceRequestRepository_Roo_Jpa_Repository {
    
    declare parents: ServiceRequestRepository extends JpaRepository<ServiceRequest, Long>;
    
    declare parents: ServiceRequestRepository extends JpaSpecificationExecutor<ServiceRequest>;
    
    declare @type: ServiceRequestRepository: @Repository;
    
}

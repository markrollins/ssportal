package net.grouplink.ssportal.repository;
import net.grouplink.ssportal.domain.ServiceType;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = ServiceType.class)
public interface ServiceTypeRepository {
}

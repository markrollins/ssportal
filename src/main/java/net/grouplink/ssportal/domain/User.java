package net.grouplink.ssportal.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.entity.RooJpaEntity;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@RooJavaBean
@RooToString
@RooJpaEntity
public class User {

    /**
     */
    @NotNull
    private String firstName;

    /**
     */
    @NotNull
    private String lastName;

    /**
     */
    @NotNull
    @Column(unique = true)
    private String email;

    /**
     */
    @NotNull
    private String password;

    /**
     */
    @ManyToOne
    private Tenant tenant;


    public String getDisplayName() {
        return lastName + ", " + firstName;
    }

}

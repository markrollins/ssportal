@FilterDef(name = "filterByTenant", parameters = @ParamDef(name = "tenantId", type = "long"))
package net.grouplink.ssportal.domain;

import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

package net.grouplink.ssportal.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.entity.RooJpaEntity;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.persistence.Column;

@RooJavaBean
@RooToString
@RooJpaEntity
public class Tenant {

    /**
     */
    @NotNull
    private String name;

    /**
     */
    private String logo;

    @NotNull
    @Column(unique = true)
    private String subdomain;
}

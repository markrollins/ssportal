package net.grouplink.ssportal.domain;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.entity.RooJpaEntity;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooJpaEntity

@Filter(name = "filterByTenant", condition = "tenant = :tenantId")
public class Service {

    /**
     */
    @NotNull
    private String name;

    /**
     */
    @NotNull
    @ManyToOne
    private ServiceType serviceType;

    @NotNull
    @ManyToOne
    private Tenant tenant;
}

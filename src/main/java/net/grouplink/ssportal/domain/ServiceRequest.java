package net.grouplink.ssportal.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.entity.RooJpaEntity;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@RooJavaBean
@RooToString
@RooJpaEntity
public class ServiceRequest {

    /**
     */
    @NotNull
    @ManyToOne
    private Service service;

    /**
     */
    @ManyToOne
    private User user;
}
